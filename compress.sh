#!/bin/sh

rm -rf create_launcher_*.tar.bz2 create_launcher_*.tar.bz2.sha256sum

FILENAME=create_launcher_`grep -m 1 -i '* version' HISTORY.md |awk {' print $3 '}`.tar.bz2

tar -cjf $FILENAME create_launcher.desktop create_launcher.glade create_launcher.js create_launcher.svg install.sh README.md HISTORY.md COPYING
sha256sum $FILENAME > ${FILENAME}.sha256sum
