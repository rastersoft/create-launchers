#!/usr/bin/env gjs

/*
 * Copyright (C) 2022 Sergio Costas (rastersoft@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

imports.gi.versions.Gtk = '3.0';
const Gtk = imports.gi.Gtk;
const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GdkPixbuf = imports.gi.GdkPixbuf;
const ByteArray = imports.byteArray;

_ = function(text) { return text; }

class CreateDesktop {
    constructor() {
        this._app = new Gtk.Application({application_id: 'com.rastersoft.createdesktop', flags: Gio.ApplicationFlags.FLAGS_NONE});;
        this._window = null;
        this._app.connect('activate', this._activate.bind(this));
        this._app.connect('startup', this._startup.bind(this));
        this._icon = null;
        this._userName = GLib.get_user_name();
        this._mainCategories = [
            [_('Audio'), 'Audio'],
            [_('Video'), 'Video'],
            [_('AudioVideo'), 'AudioVideo'],
            [_('Development'),'Development'],
            [_('Education'), 'Education'],
            [_('Game'), 'Game'],
            [_('Graphics'), 'Graphics'],
            [_('Network'), 'Network'],
            [_('Office'), 'Office'],
            [_('Settings'), 'Settings'],
            [_('System'), 'System'],
            [_('Utility'), 'Utility']];
        this._subCategories = [
            [_('Building'), 'Building', 'Development'],
            [_('Debugger'), 'Debugger', 'Development'],
            [_('IDE'), 'IDE', 'Development'],
            [_('GUI Designer'), 'GUIDesigner', 'Development'],
            [_('Profiling'), 'Profiling', 'Development'],
            [_('Revision Control'), 'RevisionControl', 'Development'],
            [_('Translation'), 'Translation', 'Development'],
            [_('Calendar'), 'Calendar', 'Office'],
            [_('Contact Management'), 'ContactManagement', 'Office'],
            [_('Database'), 'Database', 'Office/Development/AudioVideo'],
            [_('Dictionary'), 'Dictionary', 'Office;Text Tools'],
            [_('Chart'), 'Chart', 'Office'],
            [_('Email'), 'Email', 'Office;Network'],
            [_('Finance'), 'Finance', 'Office'],
            [_('FlowChart'), 'FlowChart', 'Office'],
            [_('PDA'), 'PDA', 'Office'],
            [_('Project Management'), 'ProjectManagement', 'Office;Development'],
            [_('Presentation'), 'Presentation', 'Office'],
            [_('Spreadsheet'), 'Spreadsheet', 'Office'],
            [_('Word Processor'), 'WordProcessor', 'Office'],
            [_('2D Graphics'), '2DGraphics', 'Graphics'],
            [_('Vector Graphics'), 'VectorGraphics', 'Graphics;2D Graphics'],
            [_('Raster Graphics'), 'RasterGraphics', 'Graphics;2D Graphics'],
            [_('3D Graphics'), '3DGraphics', 'Graphics'],
            [_('Scanning'), 'Scanning', 'Graphics'],
            [_('OCR'), 'OCR', 'Graphics;Scanning'],
            [_('Photography'), 'Photography', 'Graphics/Office'],
            [_('Publishing'), 'Publishing', 'Graphics/Office'],
            [_('Viewer'), 'Viewer', 'Graphics/Office'],
            [_('Text Tools'), 'TextTools', 'Utility'],
            [_('Desktop Settings'), 'DesktopSettings', 'Settings'],
            [_('Hardware Settings'), 'HardwareSettings', 'Settings'],
            [_('Printing'), 'Printing', 'Hardware Settings;Settings'],
            [_('Package Manager'), 'PackageManager', 'Settings'],
            [_('Dialup'), 'Dialup', 'Network'],
            [_('Instant Messaging'), 'InstantMessaging', 'Network'],
            [_('Chat'), 'Chat', 'Network'],
            [_('IRC Client'), 'IRCClient', 'Network'],
            [_('File Transfer'), 'FileTransfer', 'Network'],
            [_('Ham Radio'), 'HamRadio', 'Network/Audio'],
            [_('News'), 'News', 'Network'],
            [_('P2P'), 'P2P', 'Network'],
            [_('Remote Access'), 'RemoteAccess', 'Network'],
            [_('Telephony'), 'Telephony', 'Network'],
            [_('Telephony Tools'), 'TelephonyTools', 'Utility'],
            [_('VideoConference'), 'VideoConference', 'Network'],
            [_('Web Browser'), 'WebBrowser', 'Network'],
            [_('Web Development'), 'WebDevelopment', 'Network/Development'],
            [_('Midi'), 'Midi', 'AudioVideo;Audio'],
            [_('Mixer'), 'Mixer', 'AudioVideo;Audio'],
            [_('Sequencer'), 'Sequencer', 'AudioVideo;Audio'],
            [_('Tuner'), 'Tuner', 'AudioVideo;Audio'],
            [_('TV'), 'TV', 'AudioVideo;Video'],
            [_('AudioVideo Editing'), 'AudioVideoEditing', 'Audio/Video;AudioVideo'],
            [_('Player'), 'Player', 'Audio/Video;AudioVideo'],
            [_('Recorder'), 'Recorder', 'Audio/Video;AudioVideo'],
            [_('Disc Burning'), 'DiscBurning', 'AudioVideo'],
            [_('Action Game'), 'ActionGame', 'Game'],
            [_('Adventure Game'), 'AdventureGame', 'Game'],
            [_('Arcade Game'), 'ArcadeGame', 'Game'],
            [_('Board Game'), 'BoardGame', 'Game'],
            [_('Blocks Game'), 'BlocksGame', 'Game'],
            [_('Card Game'), 'CardGame', 'Game'],
            [_('Kids Game'), 'KidsGame', 'Game'],
            [_('Logic Game'), 'LogicGame', 'Game'],
            [_('RolePlaying'), 'RolePlaying', 'Game'],
            [_('Simulation'), 'Simulation', 'Game'],
            [_('Sports Game'), 'SportsGame', 'Game'],
            [_('Strategy Game'), 'StrategyGame', 'Game'],
            [_('Art'), 'Art', 'Education'],
            [_('Construction'), 'Construction', 'Education'],
            [_('Music'), 'Music', 'AudioVideo;Education'],
            [_('Languages'), 'Languages', 'Education'],
            [_('Science'), 'Science', 'Education'],
            [_('Artificial Intelligence'), 'ArtificialIntelligence', 'Education;Science'],
            [_('Astronomy'), 'Astronomy', 'Education;Science'],
            [_('Biology'), 'Biology', 'Education;Science'],
            [_('Chemistry'), 'Chemistry', 'Education;Science'],
            [_('Computer Science'), 'ComputerScience', 'Education;Science'],
            [_('Data Visualization'), 'DataVisualization', 'Education;Science'],
            [_('Economy'), 'Economy', 'Education'],
            [_('Electricity'), 'Electricity', 'Education;Science'],
            [_('Geography'), 'Geography', 'Education'],
            [_('Geology'), 'Geology', 'Education;Science'],
            [_('Geoscience'), 'Geoscience', 'Education;Science'],
            [_('History'), 'History', 'Education'],
            [_('Image Processing'), 'ImageProcessing', 'Education;Science'],
            [_('Literature'), 'Literature', 'Education'],
            [_('Math'), 'Math', 'Education;Science'],
            [_('Numerical Analysis'), 'NumericalAnalysis', 'Education;Science;Math'],
            [_('Medical Software'), 'MedicalSoftware', 'Education;Science'],
            [_('Physics'), 'Physics', 'Education;Science'],
            [_('Robotics'), 'Robotics', 'Education;Science'],
            [_('Sports'), 'Sports', 'Education'],
            [_('Parallel Computing'), 'ParallelComputing', 'Education;Science;Computer Science'],
            [_('Amusement'), 'Amusement', ''],
            [_('Archiving'), 'Archiving', 'Utility'],
            [_('Compression'), 'Compression', 'Utility;Archiving'],
            [_('Electronics'), 'Electronics', ''],
            [_('Emulator'), 'Emulator', 'System/Game'],
            [_('Engineering'), 'Engineering', ''],
            [_('File Tools'), 'FileTools', 'Utility/System'],
            [_('File Manager'), 'FileManager', 'System;File Tools'],
            [_('Terminal Emulator'), 'TerminalEmulator', 'System'],
            [_('Filesystem'), 'Filesystem', 'System'],
            [_('Monitor'), 'Monitor', 'System'],
            [_('Security'), 'Security', 'Settings/System'],
            [_('Accessibility'), 'Accessibility', 'Settings/Utility'],
            [_('Calculator'), 'Calculator', 'Utility'],
            [_('Clock'), 'Clock', 'Utility'],
            [_('Text Editor'), 'TextEditor', 'Utility'],
            [_('Documentation'), 'Documentation', ''],
            [_('Core'), 'Core', ''],
            ['KDE', 'KDE', 'QT'],
            ['GNOME', 'GNOME', 'GTK'],
            ['GTK', 'GTK', ''],
            ['Qt', 'Qt', ''],
            ['Motif', 'Motif', ''],
            ['Java', 'Java', ''],
            [_('ConsoleOnly'), 'ConsoleOnly', '']];
    }

    run() {
        this._app.run(ARGV);
    }

    _loadGlade(filenames) {
        for (let filename of filenames) {
            let file = Gio.File.new_for_path(filename);
            if (file.query_exists(null)) {
                return Gtk.Builder.new_from_file(filename);
            }
        }
        return null;
    }

    _filterExecutables(fileData) {
        if (fileData.filename.toLowerCase().endsWith('.desktop')) {
            return false;
        }
        let file = Gio.File.new_for_path(fileData.filename);
        if (!file.query_exists(null)) {
            return false;
        }
        let type = file.query_file_type(Gio.FileQueryInfoFlags.NONE, null);
        switch(type) {
        case Gio.FileType.DIRECTORY:
            return true;
        case Gio.FileType.MOUNTABLE:
        case Gio.FileType.SPECIAL:
            return false;
        }
        let fileInfo = file.query_info(Gio.FILE_ATTRIBUTE_UNIX_MODE + "," + Gio.FILE_ATTRIBUTE_OWNER_USER, Gio.FileQueryInfoFlags.NONE, null);
        let unixMode = fileInfo.get_attribute_uint32(Gio.FILE_ATTRIBUTE_UNIX_MODE);
        if ((this._userName == fileInfo.get_attribute_string(Gio.FILE_ATTRIBUTE_OWNER_USER)) & ((unixMode & 0o0100) != 0)) {
            return true;
        }
        if (unixMode & 0o01) {
            return true;
        }
        return false;
    }

    _checkExecutable() {
        if (!this._validLoadedFilename) {
            return;
        }
        let file = {filename: this._exec_file.get_filename()};
        if (!file.filename) {
            this._validFile = true;
        } else {
            this._validFile = this._filterExecutables(file);
        }
        this._invalid_executable_label.set_visible(!this._validFile);
    }

    _startup() {

        this._currentIcon = null;
        this._iconSize = 64;
        this._desktopFilePath = null;
        this._extraCategories = [];

        let builder = this._loadGlade([
            'create_launcher.glade',
            GLib.build_filenamev([GLib.get_home_dir(), '.local', 'share', 'create_launcher', 'create_launcher.glade'])]);

        this._window = builder.get_object('mainwindow');
        this._iconSelector = builder.get_object('icon_chooser');
        this._button_accept = builder.get_object('button_accept');
        this._button_icon = builder.get_object('change_icon');
        this._icon = builder.get_object('icon');
        this._launcher_name = builder.get_object('launcher_name');
        this._generic_name = builder.get_object('generic_name');
        this._exec_file = builder.get_object('exec_file');
        this._filefilter_executable = builder.get_object('filefilter_executable');
        this._filefilter_executable.add_custom(Gtk.FileFilterFlags.FILENAME, this._filterExecutables.bind(this));
        this._category = builder.get_object('category');
        this._launch_from_terminal = builder.get_object('launch_from_terminal');
        this._in_applications = builder.get_object('in_applications');
        this._in_desktop = builder.get_object('in_desktop');
        this._prefersExternalGPU = builder.get_object('use_external_gpu');
        this._iconSelectorOk = builder.get_object('icon_ok');
        this._iconSelectorCancel = builder.get_object('icon_cancel');
        this._iconSelectorCancel.set_sensitive(true);
        this._destination_label = builder.get_object('destination_label');
        this._invalid_executable_label = builder.get_object('invalid_executable_label');
        this._extraCategoriesListStore = builder.get_object('liststore2');

        this._exec_parameters = builder.get_object('exec_parameters');
        this._keywords = builder.get_object('keywords');
        this._program_path = builder.get_object('path');
        this._comment = builder.get_object('comment');
        this._startup_notify = builder.get_object('startup_notify');

        let header = this._iconSelector.get_header_bar();
        header.set_show_close_button(false);
        this._previewWidget = new Gtk.Image();
        this._iconSelector.set_preview_widget(this._previewWidget);

        this._iconSelector.set_current_folder(GLib.get_home_dir());
        this._exec_file.set_current_folder(GLib.get_home_dir());

        this._category.set_id_column(1);
        this._iconSelector.set_attached_to(this._button_icon);
        this._iconSelector.set_modal(true);
        this._app.add_window(this._window);
        builder.connect_signals_full(this._connectSignals.bind(this));

        this._appsFolder = GLib.build_filenamev([GLib.get_home_dir(), '.local', 'share', 'applications']);
        this._validLoadedFilename = true;
        if (ARGV.length > 0) {
            if (ARGV[0].toLowerCase().endsWith(".desktop")) {
                this._loadDesktopFile(GLib.canonicalize_filename(ARGV[0], null), true);
                if (this._desktopFilePath === GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP)) {
                    this._in_desktop.set_active(true);
                    this._desktopFilePath = null;
                }
                if (this._desktopFilePath === this._appsFolder) {
                    this._in_applications.set_active(true);
                    this._desktopFilePath = null;
                }
            } else {
                this._exec_file.set_filename(Gio.File.new_for_path(ARGV[0]).get_path());
                this._checkIsAppImage();
            }
        }
        this._checkExecutable();
        this.refreshStatus();
        for (let entry of this._mainCategories) {
            let iter = this._extraCategoriesListStore.append();
            this._extraCategoriesListStore.set(iter,
                [0, 1, 2, 3],
                [this._extraCategories.indexOf(entry[1]) != -1, entry[0], entry[1], '']);
        }
        for (let entry of this._subCategories) {
            let iter = this._extraCategoriesListStore.append();
            this._extraCategoriesListStore.set(iter,
                [0, 1, 2, 3],
                [this._extraCategories.indexOf(entry[1]) != -1, entry[0], entry[1], entry[2]]);
        }
    }

    _loadDataFromDesktopFile(filename) {
        let content = null;
        try {
            let file = Gio.File.new_for_path(filename);
            if (!file.query_exists(null)) {
                return null;
            }
            let [ok, data, etag] = file.load_contents(null);
            content = ByteArray.toString(data);
        } catch(e) {
            return null;
        }
        let entries = {};
        for(let entry of content.split("\n")) {
            let pos = entry.indexOf("=");
            if (pos == -1) {
                continue;
            }
            entries[entry.substring(0, pos)] = entry.substring(pos+1);
        }
        return entries;
    }

    _loadDesktopFile(filename, overwriteExecutable) {
        let entries = this._loadDataFromDesktopFile(filename);
        if (!entries) {
            return;
        }
        if ('GenericName' in entries) {
            this._generic_name.set_text(entries['GenericName']);
        }
        if ('Name' in entries) {
            this._launcher_name.set_text(entries['Name']);
        }
        if (('Exec' in entries) && overwriteExecutable) {
            let exec = entries['Exec'];
            let params = '';
            let pos = exec.indexOf(' ');
            if (pos != -1) {
                params = exec.substring(pos+1);
                exec = exec.substring(0, pos);
            }
            if (exec[0] !== '/') {
                this._validFile = false;
                this._invalid_executable_label.set_visible(true);
                this._validLoadedFilename = false;
            } else {
                this._exec_file.set_filename(exec);
                this._exec_parameters.set_text(params);
            }
        }
        if ('Comment' in entries) {
            this._comment.set_text(entries['Comment']);
        }
        if ('Keywords' in entries) {
            this._keywords.set_text(entries['Keywords']);
        }
        if ('StartupNotify' in entries) {
            this._startup_notify.set_active(entries['StartupNotify'] == 'true');
        }
        if ('Path' in entries) {
            this._program_path.set_text(entries['Path']);
        }
        if ('Categories' in entries) {
            let setCategory = false;
            for (let category of entries['Categories'].split(';')) {
                category = category.trim();
                if ((!setCategory) && this._category.set_active_id(category)) {
                    setCategory = true;
                }
                this._extraCategories.push(category);
            }
        }
        if ('Icon' in entries) {
            this._setCurrentIcon(entries['Icon']);
        }
        this._prefersExternalGPU.set_active(this._getProperty(entries, 'PrefersNonDefaultGPU'));
        this._launch_from_terminal.set_active(this._getProperty(entries, 'Terminal'));

        if (overwriteExecutable) {
            this._desktopFileFull = filename;
            this._desktopFilePath = GLib.path_get_dirname(filename);
            this._desktopFileName = GLib.path_get_basename(filename);
        } else {
            this._desktopFileFull = null;
            this._desktopFileName = null;
            this._desktopFilePath = null;
        }
    }

    _getProperty(entries, property) {
        if (property in entries) {
            return (entries[property] == 'true');
        } else {
            return false;
        }
    }

    _activate() {
        this._window.show_all();
        if (this._desktopFileFull) {
            if (this._desktopFilePath) {
                this._destination_label.set_label(this._desktopFileFull);
            } else {
                this._destination_label.set_label(this._desktopFileName);
            }
        } else {
            this._destination_label.hide();
        }
        this._window.present();
        this._checkExecutable();
        this.refreshStatus();
    }

    refreshStatus() {
        let enableAccept = this._validFile;
        if (this._launcher_name.get_text() == '') {
            enableAccept = false;
        }
        if (this._exec_file.get_filename() == '') {
            enableAccept = false;
        }
        if (!this._category.get_active_id()) {
            enableAccept = false;
        }
        if ((!this._in_applications.get_active()) && (!this._in_desktop.get_active()) && (this._desktopFilePath === null)) {
            enableAccept = false;
        }
        this._button_accept.set_sensitive(enableAccept);
    }

    on_cell_toggled(widget, path) {
        this.refreshStatus();
        let [ok,iter] = this._extraCategoriesListStore.get_iter_from_string(path);
        let value = this._extraCategoriesListStore.get_value(iter, 0);
        this._extraCategoriesListStore.set_value(iter, 0, !value);
    }

    _generateLauncher(destinationPath, setTrusted) {
        let name = this._launcher_name.get_text();
        let name_no_odd_chars = name.replace('/', '_').replace('\\', '_');
        let fullPath = this._exec_file.get_filename();
        let exec = fullPath;
        let exec_params = this._exec_parameters.get_text();
        if (exec_params != '') {
            exec += ` ${exec_params}`;
        }
        let runningPath = this._program_path.get_text();
        let useTerminal = this._launch_from_terminal.get_active() ? "true" : "false";
        let startupNotify = this._startup_notify.get_active() ? "true" : "false";
        let mainCategory = this._category.get_active_id();
        let foundAudioOrVideo = (mainCategory == 'Audio') || (mainCategory == 'Video');
        let foundAudioVideo = false;
        let categories = `${mainCategory};`;
        for (let [ok, iter] = this._extraCategoriesListStore.get_iter_first(); ok ; ok = this._extraCategoriesListStore.iter_next(iter)) {
            let value = this._extraCategoriesListStore.get_value(iter, 2);
            if (this._extraCategoriesListStore.get_value(iter, 0)) {
                if (value != mainCategory) {
                    categories += `${value};`;
                    continue;
                }
                if ((value == 'Audio') || (value == 'Video')) {
                    foundAudioOrVideo = true;
                }
                if (value == 'AudioVideo') {
                    foundAudioVideo = true;
                }
            }
        }
        if (foundAudioOrVideo && (!foundAudioVideo)) {
            categories += ';AudioVideo';
        }
        let iconPath = this._currentIcon;
        let content = `[Desktop Entry]

Type=Application
Version=1.5
Name=${name}
Path=${runningPath}
TryExec=${fullPath}
Exec=${exec}
Terminal=${useTerminal}
Categories=${categories}
StartupNotify=${startupNotify}
`;
        if (this._currentIcon) {
            content += `Icon=${iconPath}\n`;
        }
        if (this._prefersExternalGPU.get_active()) {
            content += 'PrefersNonDefaultGPU=true\n';
        }
        let comment = this._convertLocalestring(this._comment.get_text());
        if (comment != '') {
            content += `Comment=${comment}\n`;
        }
        let keywords = this._keywords.get_text();
        if (keywords != '') {
            content += `Keywords=${keywords}`;
        }
        let genericName = this._generic_name.get_text();
        if (genericName != '') {
            content += `GenericName=${genericName}`;
        }
        if (!this._desktopFileName) {
            this._desktopFileName = `${name_no_odd_chars}.desktop`;
            this._destination_label.set_label(this._desktopFileName);
            this._destination_label.show();
        }
        let destinationFile = GLib.build_filenamev([destinationPath, this._desktopFileName]);
        let file = Gio.File.new_for_path(destinationFile);
        if (file.query_exists(null)) {
            file.delete(null);
        }
        if (GLib.mkdir_with_parents(file.get_parent().get_path(), 0o744) === 0) {
            file.replace_contents(content, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
        }

        let info = new Gio.FileInfo();
        info.set_attribute_uint32(Gio.FILE_ATTRIBUTE_UNIX_MODE, 0o00755);
        if (setTrusted) {
            info.set_attribute_string('metadata::trusted', 'true');
        }
        file.set_attributes_from_info(info, Gio.FileQueryInfoFlags.NONE, null);
    }

    _convertLocalestring(text) {
        return text.replace('\\', '\\\\');
    }

    on_button_accept_clicked() {
        this._checkExecutable();
        if (!this._validFile) {
            this.refreshStatus();
            return;
        }
        if (this._desktopFilePath) {
            this._generateLauncher(this._desktopFilePath, true);
        }
        if (this._in_applications.get_active()) {
            this._generateLauncher(this._appsFolder, false);
        }
        if (this._in_desktop.get_active()) {
            this._generateLauncher(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP), true);
        }
        this._button_accept.set_sensitive(false);
    }

    on_close_button_clicked() {
        this._window.close();
    }

    on_change_icon_clicked() {
        this._iconSelector.show_all();
        this._updateIconChooserStatus();
    }

    _getAllChildren(path, recursive = true) {
        let folder = Gio.File.new_for_path(path);
        let enumerator = folder.enumerate_children("", Gio.FileQueryInfoFlags.NONE, null);
        let paths = [];
        while(true) {
            let child = enumerator.next_file(null);
            if (child == null) {
                break;
            }
            let name = child.get_name();
            if (name == null) {
                continue;
            }
            let fullPath = GLib.build_filenamev([path, name]);
            if (child.get_file_type() == Gio.FileType.DIRECTORY) {
                if (recursive) {
                    paths = paths.concat(this._getAllChildren(fullPath));
                }
            } else {
                paths.push([name, fullPath]);
            }
        }
        return paths;
    }

    _checkIsAppImage() {
        let executable = this._exec_file.get_filename();
        if (!executable) {
            return;
        }
        if (!executable.toLowerCase().endsWith(".appimage")) {
            return;
        }
        let subprocess = Gio.Subprocess.new([executable, "--appimage-mount"], Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE);
        let stdoutStream = new Gio.DataInputStream({
            base_stream: subprocess.get_stdout_pipe(),
            close_base_stream: true
        });
        let path = null;
        try {
            let l = 0;
            [path, l] = stdoutStream.read_line(null);
        } catch (e) {
            print(`Error: ${e}; ${e.message}; ${e.stack}`);
        }
        if (!path) {
            return;
        }
        path = ByteArray.toString(path);
        let rootFiles = this._getAllChildren(path, false);
        let foundDesktop = false;
        let foundIcon = null;
        for (let file of rootFiles) {
            if (file[0].endsWith(".desktop")) {
                this._loadDesktopFile(file[1], false);
                foundDesktop = true;
                continue;
            }
            if (file[0].endsWith(".svg")) {
                foundIcon = file;
                continue;
            }
            if ((foundIcon === null) && (file[0].endsWith(".png"))) {
                foundIcon = file;
                continue;
            }
        }
        if (foundIcon !== null) {
            let origin = Gio.File.new_for_path(foundIcon[1]);
            let destination = Gio.File.new_for_path(GLib.build_filenamev([GLib.get_user_data_dir(), "icons", foundIcon[0]]));
            let dest_folder = destination.get_parent();
            if (!dest_folder.query_exists(null))
                dest_folder.make_directory_with_parents(null);
            origin.copy(destination, Gio.FileCopyFlags.OVERWRITE, null, null);
            this._setCurrentIcon(destination.get_path());
        }
        if (!foundDesktop) {
            let desktopFiles = this._getAllChildren(GLib.build_filenamev([path, "usr", "share", "applications"]));
            if (desktopFiles.length == 0) {
                return;
            }
            this._loadDesktopFile(desktopFiles[0][1], false);
            let entries = this._loadDataFromDesktopFile(desktopFiles[0][1]);
            if (('Icon' in entries) && (foundIcon === null)) {
                let origin_icon_path = GLib.build_filenamev([path, "usr", "share", "icons", "*"]);
                let destination_icon_path = GLib.build_filenamev([GLib.get_home_dir(), ".local", "share", "icons"]);
                let command_line = `cp -a ${origin_icon_path} ${destination_icon_path}`;
                GLib.spawn_command_line_sync(command_line);
                let iconsFiles= this._getAllChildren(destination_icon_path);
                for (let [name, path] of iconsFiles) {
                    if (name.startsWith(entries['Icon'])) {
                        this._setCurrentIcon(path);
                        break;
                    }
                }
            }
        }
        subprocess.force_exit();
    }

    on_exec_file_set() {
        this._validLoadedFilename = true;
        this._checkIsAppImage();
        this.refreshStatus();
        this._checkExecutable();
        this._iconSelector.set_current_folder(this._exec_file.get_current_folder());
        this._program_path.set_text(this._exec_file.get_current_folder());
    }

    on_icon_ok_clicked() {
        this._selectedIcon();
    }

    on_icon_cancel_clicked() {
        this._iconSelector.hide();
    }

    on_icon_chooser_file_activated() {
        this._selectedIcon();
    }

    on_icon_chooser_selection_changed() {
        this._updateIconChooserStatus();
        this.refreshStatus();
    }

    _updateIconChooserStatus() {
        let file = this._iconSelector.get_file();
        let validFile = file && !(file.query_file_type(Gio.FileQueryInfoFlags.NONE, null) == Gio.FileType.DIRECTORY);
        this._iconSelectorOk.set_sensitive(validFile);
        this._iconSelector.set_preview_widget_active(validFile);
        if (validFile) {
            let pixbuf = this._getPixbufFromFile(file.get_path(), this._previewWidget.get_scale_factor());
            if (pixbuf) {
                this._previewWidget.set_from_surface(pixbuf);
            }

        }
    }

    _connectSignals (builder, object, signalName, handlerName, connectObject, flags) {
        object.connect(signalName, this[handlerName].bind(this));
    }

    _selectedIcon() {
        this._setCurrentIcon(this._iconSelector.get_filename());
        this._iconSelector.hide();
    }

    _setCurrentIcon(filename) {
        this._currentIcon = filename;
        let pixbuf = this._getPixbufFromFile(filename, this._icon.get_scale_factor());
        if (pixbuf) {
            this._icon.set_from_surface(pixbuf);
        }
    }

    _getPixbufFromFile(file, scale) {
        try {
            var pixbuf = GdkPixbuf.Pixbuf.new_from_file(file);
        } catch(e) {
            return null;
        }
        let w = pixbuf.get_width();
        let h = pixbuf.get_height();
        if (w > h) {
            h = h * this._iconSize / w;
            w = this._iconSize;
        } else {
            w = w * this._iconSize / h;
            h = this._iconSize;
        }
        return Gdk.cairo_surface_create_from_pixbuf(pixbuf.scale_simple(w * scale, h * scale, GdkPixbuf.InterpType.BILINEAR), scale, null);
    }
}

let app = new CreateDesktop();
app.run();
