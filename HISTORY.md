# HISTORY OF VERSIONS #

* Version 0.99.4 (2022-04-30)
  * Better .appimage analysis (Sergio Costas)

* Version 0.99.3 (2022-04-25)
  * Analyze .appimage files and import .desktop and icons (Sergio Costas)

* Version 0.99.2 (2022-03-10)
  * Keep the window optimum size (Sundeep Mediratta)
  * Better icon chooser appearance (Sundeep Mediratta)
  * Now filters the executables in the file selector (Sergio Costas)
  * Now notifies when the executable is not valid during edition (Sergio Costas)
  * Now preserves the filename during edition (Sergio Costas)
  * Added Generic name entry (Sergio Costas)
  * Added support for subcategories (Sergio Costas)

* Version 0.99.1 (2022-03-05)
  * Start the icon selection in the same folder than the binary
  * Added support for advanced options
  * Now detects if it is editing a .desktop file from the desktop or the applications folder

* Version 0.99 (2022-03-04)
  * First public version
